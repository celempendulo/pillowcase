............................................................................................................... TEMPLATES


############################################
#LIST       - must have closest data-object
############################################

<ul data-list='person.stories.saved'></ul>


###########################################
#OBJECT     - no scoping required
###########################################

<li data-object='{{post._id}}'>{{>story}}</li>


###########################################
#KEY        - must have closest data-object
###########################################

<span data-key='creator.name'>{{creator.name}}</span>


............................................................................................................... REQUESTS

##########################################
#NAVIGATING     @pending|success|error
##########################################

<button formmethod='get|post|put|patch|delete' formaction='#state|page.html?user=123' cache='true'></button>


#########################################
#FORMING        @pending|success|error
#########################################

<form method='get|post|put|patch|delete' action='endpoint.com' cache='true'></form>


............................................................................................................... JAVASCRIPT


##################################################################
#STATES - JS
##################################################################

import {Observe} from '../events/publisher.js';

/**@news-item*/

Observe.when('#signUpForm@success', 'user-is-authenticated');
Observe.when('#meme@click.reaction-1', 'like-button-pressed');
Observe.when('#meme@click.reaction-2', 'dislike-button-pressed');
Observe.when('#meme@click.reaction-3', 'share-button-pressed');


##################################################################
#STATES - CSS
##################################################################


.news-item {

	---user-is-authenticated: 'back replace save show hide first last remove filter prepend append replace merge scrollIntoView start finish set unset toggle';
	--like-button-pressed: 'append request save';
	--dislike-button-pressed: 'append request save';
	--share-button-pressed: 'append request save';
	animation: 'crayon--noted' 0.1ms;
}