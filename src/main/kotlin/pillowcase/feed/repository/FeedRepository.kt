package pillowcase.feed.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.feed.Feed
import java.util.*

interface FeedRepository: ArangoRepository<Feed, String> {

    fun findByAccountIdOrderByDateDesc(id: String): Optional<Feed>
}