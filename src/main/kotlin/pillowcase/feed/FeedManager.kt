package pillowcase.feed

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.feed.repository.FeedRepository
import pillowcase.post.Post
import pillowcase.post.PostManager

@Component
class FeedManager @Autowired constructor(
        private val repository: FeedRepository,
        private val postManager: PostManager) {



    fun get(account: Account): List<Post> {
        repository.findByAccountIdOrderByDateDesc(account.id!!).let {
            if(it.isPresent) {
                repository.delete(it.get())
                return postManager.get(it.get().posts)
            }
        }
        return listOf()
    }


    fun add(account: Account, posts: List<Post>) {
        val ids = posts.map { it.id!! }
        repository.save(Feed(ids, account))
    }
}