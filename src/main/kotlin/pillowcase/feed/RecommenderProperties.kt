package pillowcase.feed

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "pillowcase.recommender")
class RecommenderProperties(val batchSize: Int = 7) {

}