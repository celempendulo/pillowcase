package pillowcase.feed

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Relations
import epsilon.accounts.Account
import org.springframework.data.annotation.Id
import pillowcase.edges.FeedOwner
import java.time.LocalDateTime

@Document("feed")
class Feed(val posts: List<String>, @Relations(edges = [FeedOwner::class])val account:Account,
           val date:LocalDateTime = LocalDateTime.now(), @Id var id: String? = null)