package pillowcase.api.model

class GeneralResponse(val success: Boolean = true, val message: String? = null)