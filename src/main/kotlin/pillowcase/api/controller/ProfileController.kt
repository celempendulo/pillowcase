package pillowcase.api.controller

import epsilon.accounts.AccountManager
import epsilon.failure.ClientError
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import pillowcase.api.model.GeneralResponse
import pillowcase.blocking.BlockingManager
import pillowcase.following.FollowingManager
import pillowcase.post.PostManager
import pillowcase.post.saved.SavedPostManager
import pillowcase.profile.ProfileManager


@Suppress("unused")
@RestController
class ProfileController constructor(
        private val accountManager: AccountManager,
        private val blockingManager: BlockingManager,
        private val followingManager: FollowingManager,
        private val profileManager: ProfileManager,
        private val postManager: PostManager,
        private val savedPostManager: SavedPostManager) {


    @PostMapping("/follow")
    @ResponseBody
    fun follow(@RequestBody profileId: String, authentication: Authentication): GeneralResponse {
        return try {
            val blocker = profileManager.getByAccount(authentication.name)
            val profile = profileManager.get(profileId)
            followingManager.follow(blocker, profile)
            GeneralResponse()
        }
        catch(error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @PostMapping("/unfollow")
    @ResponseBody
    fun unfollow(@RequestBody profileId: String, authentication: Authentication): GeneralResponse {
        return try {
            val blocker = profileManager.getByAccount(authentication.name)
            val profile = profileManager.get(profileId)
            followingManager.unfollow(blocker, profile)
            GeneralResponse()
        }
        catch(error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @PostMapping("/block")
    @ResponseBody
    fun block(@RequestBody profileId: String, authentication: Authentication): GeneralResponse {
        return try {
            val blocker = profileManager.getByAccount(authentication.name)
            val profile = profileManager.get(profileId)
            blockingManager.block(blocker, profile)
            GeneralResponse()
        }
        catch(error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @PostMapping("/unblock")
    @ResponseBody
    fun unblock(@RequestBody profileId: String, authentication: Authentication): GeneralResponse {
        return try {
            val blocker = profileManager.getByAccount(authentication.name)
            val profile = profileManager.get(profileId)
            blockingManager.unblock(blocker, profile)
            GeneralResponse()
        }
        catch(error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @PostMapping("/savePost")
    @ResponseBody
    fun savePost(@RequestBody postId: String, authentication: Authentication): GeneralResponse {

        return try {
            val profile = profileManager.getByAccount(authentication.name)
            val post = postManager.forceGet(postId)
            savedPostManager.add(profile, post)
            GeneralResponse()
        }
        catch (error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @PostMapping("/unsavePost")
    @ResponseBody
    fun unsavePost(@RequestBody postId: String, authentication: Authentication): GeneralResponse {

        return try {
            val profile = profileManager.getByAccount(authentication.name)
            val post = postManager.forceGet(postId)
            savedPostManager.delete(profile, post)
            GeneralResponse()
        }
        catch (error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

}