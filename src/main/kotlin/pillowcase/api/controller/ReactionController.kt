package pillowcase.api.controller

import epsilon.accounts.AccountManager
import pillowcase.post.PostManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import pillowcase.interactions.InteractionManager
import pillowcase.interactions.ReactionType


@Suppress("unused")
@PreAuthorize(AccountController.HAS_USER_ROLE)
@RestController
class ReactionController @Autowired constructor(
        private val accountManager: AccountManager,
        private val postManager: PostManager,
        private val interactionManager: InteractionManager) {


    class ReactionRequest(val reaction: ReactionType, val id:String)

    @PostMapping("/addReaction")
    fun add(info: ReactionRequest, authentication: Authentication) {
        val account = accountManager.forceGet(authentication.name)
        val post = postManager.forceGet(info.id)
        interactionManager.react(post, info.reaction, account)
    }

    @DeleteMapping("/removeReaction")
    fun delete(id:String, authentication: Authentication){
        val account = accountManager.forceGet(authentication.name)
        val post = postManager.forceGet(id)
        interactionManager.unreact(post, account)
    }

}