package pillowcase.api.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import pillowcase.api.service.NotificationService
import javax.servlet.http.HttpServletResponse


@RestController
class NotificationController  @Autowired constructor(
        private val service: NotificationService) {

    @GetMapping("/notifications")
    fun register(response: HttpServletResponse, authentication: Authentication): SseEmitter {
        response.setHeader("Cache-Control", "no-store")
        val emitter = SseEmitter()
        service.addEmitter(authentication.name, emitter)
        return emitter
    }

}