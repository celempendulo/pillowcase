package pillowcase.api.controller

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import epsilon.failure.ClientError
import epsilon.localization.Language
import epsilon.localization.Messages
import epsilon.localization.Tag
import epsilon.security.auth.LoginAttemptService
import epsilon.tokens.jwt.TokenAdmin
import epsilon.validation.Validator
import pillowcase.visitor.VisitorManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import pillowcase.api.Urls
import pillowcase.api.model.GeneralResponse
import pillowcase.security.SecurityUtils
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestController
@Suppress("unused")
class AccountController @Autowired constructor(
        private val accountManager: AccountManager,
        private val visitorManager: VisitorManager,
        private val tokenAdmin: TokenAdmin,
        private val passwordEncoder: PasswordEncoder,
        private val messages: Messages,
        private val authenticationManager: AuthenticationManager,
        private val loginAttemptService: LoginAttemptService,
        private val security: SecurityUtils) {

    data class RegisterRequest(val handler: String, val password: String, val pp: String,
                               var language: Language= Language.ENGLISH)

    class LoginRequest(val handler: String, val password:String)

    class UpdateRequest(var email: String?=null, var oldPassword: String? = null, var password: String?=null,
                        var pp: String? = null, val languages: List<Language>? = null)

    class ConfirmEmailRequest(val id: String, val code: String, val email: String)

    class ResetPasswordRequest(val id: String, val password: String, val code: String)

    class UpdatePasswordRequest(val oldPassword: String, val newPassword: String)

    class UpdateResponse(var email:Boolean?= null, var password: Boolean?= null, var pp:Boolean?= null,
                         var languages:Boolean? = null)

    companion object {
       const val HAS_USER_ROLE = """hasRole('USER')"""
    }


    @PostMapping(Urls.REGISTRATION)
    @ResponseBody
    fun register(@RequestBody info: RegisterRequest, authentication: Authentication,
                 response: HttpServletResponse): GeneralResponse {
        try {
            var account = Account(
                    password = info.password,
                    handler = info.handler,
                    pp = info.pp,
                    languages = mutableListOf(info.language),
                    active = true) // We want to immediately allow the user to interact with the application

            account = accountManager.create(account)
            afterSuccessLogin(authentication.name, account, response)
            return GeneralResponse()
        }
        catch(error: ClientError) {
            return GeneralResponse(false, error.message)
        }
    }

    @PostMapping(Urls.LOGIN)
    @ResponseBody
    fun signIn(@RequestBody info: LoginRequest, authentication: Authentication,
              response: HttpServletResponse): GeneralResponse {
        if(loginAttemptService.isAttemptExceeded(info.handler)) {
            throw ClientError(messages[Tag.Error.LOGIN_ATTEMPTS_EXCEEDED])
        }
        return try {
            val auth = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(info.handler, info.password))
            val account = accountManager.get(auth.name).get()
            afterSuccessLogin(authentication.name, account, response)
            GeneralResponse()
        }
        catch (ex: BadCredentialsException) {
            GeneralResponse(false, messages[Tag.Error.ACCOUNT_INCORRECT_CREDENTIALS])
        }
    }

    @PostMapping(Urls.UPDATE_PASSWORD)
    @ResponseBody
    fun updatePassword(@RequestBody info: UpdatePasswordRequest, authentication: Authentication): GeneralResponse {
        try {
            accountManager.get(authentication.name).get().let {
                if(loginAttemptService.isAttemptExceeded(authentication.name)) {
                    throw ClientError(messages[Tag.Error.PASSWORD_ENTRIES_EXCEEDED])
                }
                if (!Validator.isValidPassword(info.newPassword)) {
                    throw ClientError(messages[Tag.Error.INVALID_PASSWORD_FORMAT])
                }
                if (!passwordEncoder.matches(info.oldPassword, it.password)) {
                    loginAttemptService.failed(authentication.name)
                    throw ClientError(messages[Tag.Error.INCORRECT_PASSWORD])
                }
                it.password = passwordEncoder.encode(info.newPassword)
                accountManager.update(it)
                return GeneralResponse()
            }
        }
        catch (error: ClientError) {
            return GeneralResponse(false, error.message)
        }
    }

    @PostMapping(Urls.UPDATE_PICTURE)
    @ResponseBody
    fun updatePp(@RequestBody pp: String, authentication: Authentication): GeneralResponse {
        try {
            accountManager.get(authentication.name).get().let {
                it.pp = pp
                accountManager.update(it)
                return GeneralResponse()
            }
        }
        catch (error: ClientError) {
            return GeneralResponse(false, error.message)
        }
    }

    @PostMapping(Urls.UPDATE_LANGUAGES)
    @ResponseBody
    fun updateLanguages(@RequestBody languages: MutableList<Language>, authentication: Authentication): GeneralResponse {
        try {
            accountManager.get(authentication.name).get().let {
                it.languages = languages
                accountManager.update(it)
                return GeneralResponse()
            }
        }
        catch (error: ClientError) {
            return GeneralResponse(false, error.message)
        }
    }

    @PostMapping(Urls.UPDATE_EMAIL)
    @ResponseBody
    fun updateEmail(@RequestBody email: String, authentication: Authentication): GeneralResponse {
        try {
            accountManager.get(authentication.name).get().let {
                if (!Validator.isValidEmail(email)) {
                    throw ClientError(messages[Tag.Error.INVALID_EMAIL_FORMAT])
                }
                accountManager.setEmail(it.id!!, email)
                return GeneralResponse()
            }
        }
        catch (error: ClientError) {
            return GeneralResponse(false, error.message)
        }
    }


    @PostMapping(Urls.UPDATE_ACCOUNT)
    @ResponseBody
    fun update(@RequestBody info: UpdateRequest, authentication: Authentication): GeneralResponse {

        try {
            accountManager.get(authentication.name).get().let {
                if (info.password != null) {
                    if (!Validator.isValidPassword(info.password!!)) {
                        throw ClientError(messages[Tag.Error.INVALID_PASSWORD_FORMAT])
                    }
                    if (!passwordEncoder.matches(info.oldPassword, it.password)) {
                        throw ClientError(messages[Tag.Error.INCORRECT_PASSWORD])
                    }
                    it.password = passwordEncoder.encode(info.password)
                }
                if (info.pp != null) {
                    it.pp = info.pp
                }

                if (info.languages != null) {
                    it.languages = info.languages
                }
                if (info.email != null) {
                    if (!Validator.isValidEmail(info.email!!)) {
                        throw ClientError(messages[Tag.Error.INVALID_EMAIL_FORMAT])
                    }
                    accountManager.setEmail(it.id!!, info.email!!)
                }
                accountManager.update(it)
                return GeneralResponse()
            }
        }
        catch (error: ClientError) {
            return GeneralResponse(false, error.message)
        }

    }


    @PostMapping(Urls.CONFIRM_EMAIL)
    fun verifyEmail(@RequestBody info: ConfirmEmailRequest): GeneralResponse {
        return try {
            accountManager.verifyEmail(info.id, info.code, info.email)
            GeneralResponse()
        }
        catch (exception: ClientError) {
            GeneralResponse(false, exception.message)
        }
    }

    @GetMapping(Urls.RESET_PASSWORD)
    @ResponseBody
    fun displayResetPasswordPage(modelAndView: ModelAndView, id: String, code: String): ModelAndView {

        modelAndView.addObject("code", code)
        modelAndView.viewName = "resetPassword"
        return modelAndView
    }


    @GetMapping(Urls.FORGOT_PASSWORD)
    @ResponseBody
    fun forgotPassword(identifier: String): GeneralResponse {
        return try {
            val isEmail = Validator.isValidEmail(identifier)
            val account = if(isEmail) accountManager.getByEmail(identifier) else accountManager.getByHandler(identifier)
            if (account.isEmpty) {
                throw ClientError(messages[Tag.Error.ACCOUNT_NOT_FOUND])
            }
            accountManager.forgotPassword(account.get().id!!)
            GeneralResponse(true, messages[Tag.Success.ACCOUNT_PASSWORD_RESET_LINK_SENT])
        }
        catch (error: ClientError) {
            GeneralResponse(false, error.message)
        }
    }

    @GetMapping(Urls.MYSELF)
    @ResponseBody
    fun getMe(authentication: Authentication): Account {
        return accountManager.forceGet(authentication.name)
    }

    @PostMapping(Urls.RESET_PASSWORD)
    @ResponseBody
    fun resetPassword(info: ResetPasswordRequest): GeneralResponse {
        return try {
            accountManager.resetPassword(info.id, info.code, info.password)
            GeneralResponse()
        }
        catch (error: ClientError) {
            GeneralResponse(false, error.message)
        }

    }

    @GetMapping(Urls.MY_ID)
    @ResponseBody
    fun whoAmI(authentication: Authentication): String {
        return authentication.name
    }


    @GetMapping(Urls.LOGOUT)
    @ResponseBody
    fun signOut(request: HttpServletRequest, response: HttpServletResponse,
            authentication: Authentication): GeneralResponse {
        val token = security.resolveToken(request)
        if(token != null && token.type == SecurityUtils.PillowcaseToken.Type.JWT) {
            tokenAdmin.blacklist(token.value)
            security.deleteCookie(SecurityUtils.PillowcaseToken.Type.JWT.name, response)
        }
        return GeneralResponse()
    }

    private fun afterSuccessLogin(visitorId: String, account: Account, response: HttpServletResponse) {
        val jwt = tokenAdmin.generateToken(account.id!!)
        visitorManager.delete(visitorId)
        security.replaceVisitorWithUser(jwt, response)
    }


}