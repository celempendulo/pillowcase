package pillowcase.api.controller

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import epsilon.failure.ClientError
import epsilon.localization.Language
import epsilon.localization.Messages
import epsilon.localization.Tag
import pillowcase.post.Post
import pillowcase.post.PostManager
import pillowcase.post.Search
import pillowcase.reports.Report
import pillowcase.reports.ReportManager
import pillowcase.visitor.VisitorManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.web.bind.annotation.*
import pillowcase.interactions.InteractionManager
import pillowcase.post.Media
import pillowcase.profile.ProfileManager
import pillowcase.search.SearchManager


@Suppress("unused")
@RestController
class PostController @Autowired constructor(
        private val accountManager: AccountManager,
        private val messages: Messages,
        private val visitorManager: VisitorManager,
        private val postManager: PostManager,
        private val reportManager: ReportManager,
        private val interactionManager: InteractionManager,
        private val profileManager: ProfileManager,
        private val searchManager: SearchManager) {

    class AddRequest(val category: String,val topic: String, val language:Language?= null, val media: Media)
    class UpdateRequest(val id:String, val category: String, val topic: String)
    class ReportRequest(val id: String, val reason: Report.Reason)
    class SearchResponse(val unseen: List<Post> = listOf(), val seen:List<Post> = listOf())

    @PostMapping("/post")
    @ResponseBody
    fun add(@RequestBody request: AddRequest, authentication: Authentication): Post? {

        val profile = profileManager.getByAccount(authentication.name)
        val language = request.language ?: profile.writingLanguage
        val post = Post(category = request.category, topic = request.topic, language = language, owner = profile.account,
        checksum = "")
        return postManager.create(post, request.media)
    }

    @DeleteMapping("/post")
    @ResponseBody
    fun delete(@RequestBody id:String, authentication: Authentication): Post {
        accountManager.getByHandler(authentication.name).get().let {
            return postManager.delete(it, id)
        }
    }

    @PatchMapping("/post")
    @ResponseBody
    fun update(@RequestBody request: UpdateRequest, authentication: Authentication): Post {
        accountManager.getByHandler(authentication.name).get().let {
            return postManager.update(it, request.id, category = request.category, topic = request.topic)
        }
    }


    @PostMapping("/reportPost")
    fun report(info: ReportRequest ,authentication: Authentication) {
        accountManager.get(authentication.name).ifPresent { account->
            postManager.get(info.id).ifPresent { post->
                reportManager.add(account, post, info.reason)
            }
        }
    }

    @PostMapping("/search")
    @ResponseBody
    fun search(@RequestBody search: Search?, authentication: Authentication): List<Post> {
        val info = search ?: Search()
        if(authentication.authorities.contains(SimpleGrantedAuthority(Account.Roles.VISITOR.name))) {
            val visitor = visitorManager.get(authentication.name)
            if(visitor.isPresent) {
                return searchManager.search(visitor.get(), info)
            }
            /**
             * This should never happen
             */
            throw ClientError(messages[Tag.Error.INTERNAL_ERROR])
        }
        accountManager.getByHandler(authentication.name).get().let {
            return searchManager.search(it, info)
        }
    }

    @PostMapping("/viewPost")
    fun view(id: String, authentication: Authentication){

        if(authentication.authorities.contains(SimpleGrantedAuthority(Account.Roles.VISITOR.name))) {
            visitorManager.get(authentication.name).ifPresent { visitor ->
                postManager.get(id).ifPresent { post ->
                    interactionManager.view(post, visitor)
                }
            }
        }
        else {
            /**
             * If you are not a visitor, we safely assume you are a general user
             */
            accountManager.get(authentication.name).ifPresent {account->
                postManager.get(id).ifPresent {post->
                    interactionManager.view(post, account)
                }
            }
        }
    }

}