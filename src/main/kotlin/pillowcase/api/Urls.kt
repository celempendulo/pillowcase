package pillowcase.api



 object Urls {


     enum class URL(val value: String, val isGeneral: Boolean = false) {
         CONFIRM_EMAIL(Urls.CONFIRM_EMAIL),
         FORGOT_PASSWORD(Urls.FORGOT_PASSWORD),
         LOGIN(Urls.LOGIN, true),
         LOGOUT(Urls.LOGOUT),
         MYSELF(Urls.MYSELF),
         MY_ID(Urls.MY_ID, true),
         REGISTRATION(Urls.REGISTRATION, true),
         RESET_PASSWORD(Urls.RESET_PASSWORD),
         UPDATE_EMAIL(Urls.UPDATE_EMAIL),
         UPDATE_PASSWORD(Urls.UPDATE_PASSWORD),
         UPDATE_PP(Urls.UPDATE_PICTURE),
         UPDATE_LANGUAGES(Urls.UPDATE_LANGUAGES),
         UPDATE_ACCOUNT(Urls.UPDATE_ACCOUNT)
     }

     const val CONFIRM_EMAIL = "/confirmEmail"
     const val LOGIN = "/signIn"
     const val REGISTRATION = "/signUp"
     const val LOGOUT = "/signOut"
     const val FORGOT_PASSWORD = "/forgotPassword"
     const val RESET_PASSWORD = "/resetPassword"
     const val UPDATE_PASSWORD = "/updatePassword"
     const val UPDATE_PICTURE = "/updateProfilePicture"
     const val UPDATE_LANGUAGES = "/updateLanguages"
     const val UPDATE_ACCOUNT = "/updateAccount"
     const val UPDATE_EMAIL = "/updateEmail"
     const val MYSELF = "/myself"
     const val MY_ID = "/myId"                                  // what does this do bra?

     fun getVisitorUrls(): Array<String> {
         return Urls.URL.values().toList().filter { it.isGeneral }.map { it.value }.toTypedArray()
     }

     fun getUserUrls(): Array<String> {
         return Urls.URL.values().toList().filter { !it.isGeneral }.map{ it.value }.toTypedArray()
     }
}