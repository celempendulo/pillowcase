package pillowcase.api.service

import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.concurrent.ConcurrentHashMap

@Service
class NotificationService {

    private val emitters = ConcurrentHashMap<String, MutableList<SseEmitter>>()

    fun addEmitter(id: String, emitter: SseEmitter) {
        emitters[id].let {
            it?.add(emitter) ?: (emitters.put(id, mutableListOf(emitter)))
        }
        emitter.onCompletion { emitters[id]?.remove(emitter) }
        emitter.onTimeout { emitters[id]?.remove(emitter) }
    }

    fun send(id: String, data: Any, mediaType: MediaType = MediaType.APPLICATION_JSON): Boolean {
        var sent = false
        emitters[id].let { list->
            list?.forEach {
                try {
                    it.send(data, mediaType)
                    sent = true
                } catch (e: Exception) { }
            }
        }
        return sent
    }
}