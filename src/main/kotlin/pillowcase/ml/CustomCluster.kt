package pillowcase.ml

import com.arangodb.springframework.annotation.Document
import epsilon.data.Dimension
import org.springframework.data.annotation.Id
import pillowcase.edges.ClusterNode

@Document("clusters")
class CustomCluster(val center: MutableMap<Dimension, Any>, @Transient val nodes: List<ClusterNode>,
                    @Id var id: String? = null)