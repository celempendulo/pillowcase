package pillowcase.ml

import epsilon.data.Dimension

class CustomDimension {
    companion object {
        val AGE = Dimension("AGE")

        val GENDER = Dimension("DOB", Dimension.DataType.CATEGORICAL)
    }
}