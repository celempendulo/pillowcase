package pillowcase.security.auth

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import epsilon.localization.Messages
import epsilon.localization.Tag
import pillowcase.visitor.VisitorManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailServiceImpl @Autowired constructor(
        private val accountManager: AccountManager,
        private val visitorManager: VisitorManager,
        private val messages: Messages): UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails? {

        if(username.startsWith("visitors/")) {
            return findVisitor(username)
        }
        return findAccount(username)
    }


    private fun findAccount(username: String): UserDetails? {
        val container = accountManager.get(username)
        if (container.isEmpty) {
           throw UsernameNotFoundException(messages[Tag.Error.ACCOUNT_NOT_FOUND])
        }

        val account = container.get()
        val roles = mutableListOf<GrantedAuthority>()
        account.roles.forEach { role -> roles.add(SimpleGrantedAuthority(role.name)) }
        return User(username,
                account.password,
                account.active,
                true,
                true,
                !account.blocked,
                roles)
    }

    private fun findVisitor(username: String): User {
        val container = visitorManager.get(username)
        if (container.isEmpty) {
            throw UsernameNotFoundException(messages[Tag.Error.ACCOUNT_NOT_FOUND])
        }
        val roles = mutableListOf<GrantedAuthority>(SimpleGrantedAuthority(Account.Roles.VISITOR.name))
        return User(username, "", roles)
    }

}