package pillowcase.security.auth

import epsilon.tokens.jwt.TokenAdmin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.RememberMeAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import pillowcase.security.SecurityUtils
import pillowcase.visitor.VisitorManager
import pillowcase.security.SecurityUtils.PillowcaseToken
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationFilter @Autowired constructor(
        private val tokenAdmin: TokenAdmin,
        private val visitorManager: VisitorManager,
        private val userDetailsService: CustomUserDetailServiceImpl,
        private val security: SecurityUtils): OncePerRequestFilter() {


    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse,
                                  filterChain: FilterChain) {
        try {
            val token = resolveToken(request, response)
            val username = if(token.type == PillowcaseToken.Type.JWT) {
                val claims = tokenAdmin.getClaims(token.value)
                claims.body.subject
            } else {
                token.value
            }
            val user = userDetailsService.loadUserByUsername(username)
            if(user != null){
                SecurityContextHolder.getContext().authentication =
                        RememberMeAuthenticationToken(username, user, user.authorities)
            }

        }
        catch (ex: Exception) {
            SecurityContextHolder.clearContext()
            ex.printStackTrace()
            //TODO("IF TOKEN EXPIRED REMOVE IT FROM COOKIES")
            security.deleteCookie(PillowcaseToken.Type.JWT.name, response)
            //response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            handleIdentificationError(request, response)
        }

        filterChain.doFilter(request, response)
    }

    private fun handleIdentificationError(request: HttpServletRequest, response: HttpServletResponse) {
        val username = "visitors/"+createToken(response).value
        val user = userDetailsService.loadUserByUsername(username)
        if(user != null){
            SecurityContextHolder.getContext().authentication =
                    RememberMeAuthenticationToken(username, user, user.authorities)
        }

    }

    private fun resolveToken(request: HttpServletRequest, response: HttpServletResponse): PillowcaseToken {

        var token: PillowcaseToken? = security.resolveToken(request)
        if(token != null && token.type == PillowcaseToken.Type.JWT) {
            if(tokenAdmin.isBlacklisted(token.value)) {
                token = null
            }
        }
        if(token != null) return token
        /**
         * Visitor username was not provided, hence this is a new user.
         * We create an account for them and add cookie specifying username,
         * this username will be used in future.
         */
        return createToken(response)
    }

    private fun createToken(response: HttpServletResponse): PillowcaseToken {
        val visitor = visitorManager.create()
        val username = visitor.id!!
        security.addCookie(PillowcaseToken.Type.RWT.name, username, response)
        return PillowcaseToken(PillowcaseToken.Type.RWT, visitor.id!!)
    }


}