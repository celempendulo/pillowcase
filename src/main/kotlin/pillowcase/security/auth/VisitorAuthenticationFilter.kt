package pillowcase.security.auth

import pillowcase.visitor.Visitor
import pillowcase.visitor.VisitorManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.RememberMeAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class VisitorAuthenticationFilter @Autowired constructor(
        private val visitorManager: VisitorManager,
        private val userDetailsService: CustomUserDetailServiceImpl): OncePerRequestFilter() {


    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse,
                                  filterChain: FilterChain) {

        /**
         * Check if user already authenticated
         */
        if(SecurityContextHolder.getContext().authentication == null ||
                !SecurityContextHolder.getContext().authentication.isAuthenticated) {

            //Check if user is did not provide Authorization token
            val header: String? = request.getHeader("Authorization")
            if(header == null) {
                /**
                 * A user that did not provide Authorization token is marked as a visitor,
                 * we need to authenticated them as visitor.
                 */
                var username = findUsername(request)
                val visitor:Visitor
                if(username != null && username.isNotBlank()) {
                    visitor = visitorManager.createIfAbsent(username)
                    /**
                     * In the case where the provided username does not correspond to any account we created
                     * a new account, but we do not use the username provided by this user as it may not follow
                     * our username convention. We now need to update the cookie with the new username.
                     */
                    if(username != visitor.id!!) {
                        username = visitor.id!!
                        setUsername(response, username)
                    }
                    val user = userDetailsService.loadUserByUsername(username)!!
                    SecurityContextHolder.getContext().authentication =
                            AnonymousAuthenticationToken(username, user, user.authorities)
                }
                else {
                    /**
                     * Visitor username was not provided, hence this is a new user.
                     * We create an account for them and add cookie specifying username,
                     * this username will be used in future.
                     */
                    visitor = visitorManager.create()
                    username = visitor.id!!
                    val user = userDetailsService.loadUserByUsername(username)!!
                    SecurityContextHolder.getContext().authentication =
                            RememberMeAuthenticationToken(username, user, user.authorities)
                    setUsername(response, visitor.id!!)
                }
            }
        }
        filterChain.doFilter(request, response)
    }

    fun findUsername(request: HttpServletRequest): String? {
        if(request.cookies != null) {
            for(cookie in request.cookies) {
                if(cookie.name == "RWT") {
                    return cookie.value
                }
            }
        }
        return null
    }

    fun setUsername(response: HttpServletResponse, username: String) {
        response.addCookie(Cookie("RWT", username))
    }
}