package pillowcase.security

import java.security.Principal


class MoodsPrincipal constructor(val id: String, val type: Type = Type.GENERAL): Principal{

    enum class Type {
        VISITOR,
        GENERAL
    }

    override fun getName(): String {
        return id
    }
}