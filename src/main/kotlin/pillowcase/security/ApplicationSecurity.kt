package pillowcase.security

import epsilon.accounts.Account
import epsilon.security.CustomOidcUserService
import pillowcase.security.auth.CustomUserDetailServiceImpl
import pillowcase.security.auth.JwtAuthenticationFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import pillowcase.api.Urls

@Configuration
class ApplicationSecurity constructor(
        private val jwtAuthenticationFilter: JwtAuthenticationFilter,
        //private val visitorAuthenticationFilter: VisitorAuthenticationFilter,
        private val oidcUserService: CustomOidcUserService,
        private val userDetailServiceImpl: CustomUserDetailServiceImpl,
        private val jwtAuthenticationEntryPoint: AuthenticationEntryPoint,
        private val passwordEncoder: PasswordEncoder): WebSecurityConfigurerAdapter() {



    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()

                .antMatchers(*Urls.getUserUrls()).hasAnyAuthority(Account.Roles.USER.name)
                .antMatchers(*Urls.getVisitorUrls()).hasAnyAuthority(Account.Roles.VISITOR.name)
                .anyRequest().permitAll()
                .and()
                .oauth2Login().userInfoEndpoint()
                .oidcUserService(oidcUserService)
                .and()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and().addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java);

                //.addFilterAfter(visitorAuthenticationFilter, JwtAuthenticationFilter::class.java)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailServiceImpl).passwordEncoder(passwordEncoder)
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

}