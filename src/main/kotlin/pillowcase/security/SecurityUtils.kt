package pillowcase.security

import epsilon.ApplicationProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@EnableConfigurationProperties(ApplicationProperties::class)
@Component
class SecurityUtils @Autowired constructor(private val properties: ApplicationProperties) {


    //@Value("epsilon.application.production")
    //private var production: Boolean = true

    class PillowcaseToken(val type: Type, val value: String){
        enum class Type {
            JWT,
            RWT
        }
    }

    fun resolveToken(request: HttpServletRequest): PillowcaseToken? {
        if(request.cookies != null) {
            for(cookie in request.cookies) {
                if(cookie.name == PillowcaseToken.Type.JWT.name) {
                    return PillowcaseToken(PillowcaseToken.Type.JWT, cookie.value)
                }
            }
            for(cookie in request.cookies) {
                if(cookie.name == PillowcaseToken.Type.RWT.name){
                    return PillowcaseToken(PillowcaseToken.Type.RWT, cookie.value)
                }
            }
        }
        return null
    }

    fun addCookie(key: String, value: String, response: HttpServletResponse) {
        val cookie = Cookie(key, value)
        if(properties.production) {
            cookie.isHttpOnly = true
            cookie.secure = true
        }
        response.addCookie(cookie)
    }

    fun deleteCookie(key: String ,response: HttpServletResponse) {
        val cookie = Cookie(key, "")
        cookie.maxAge=0
        response.addCookie(cookie)
    }

    fun replaceVisitorWithUser(jwt: String, response: HttpServletResponse) {
        deleteCookie(PillowcaseToken.Type.RWT.name, response)
        addCookie(PillowcaseToken.Type.JWT.name, jwt, response)
    }

}