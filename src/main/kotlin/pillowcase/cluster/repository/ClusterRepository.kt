package pillowcase.cluster.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.ml.CustomCluster

interface ClusterRepository: ArangoRepository<CustomCluster, String> {
}