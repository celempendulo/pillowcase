package pillowcase.cluster

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.cluster.repository.ClusterRepository
import pillowcase.edges.repository.ClusterNodeRepository
import pillowcase.ml.CustomCluster

@Component
class ClusterManager @Autowired constructor(
        private val repository: ClusterRepository,
        private val nodeRepository: ClusterNodeRepository) {

    fun reset(clusters: List<CustomCluster>) {
        repository.deleteAll()
        clusters.forEach { cluster ->
            repository.save(cluster)
            cluster.nodes.forEach { node ->
                nodeRepository.save(node)
            }
        }
    }
}