package pillowcase.following

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import pillowcase.profile.Profile

@Edge("follows")
class Follows(@From val from: Profile, @To val to: Profile, @Id var id: String? = null)