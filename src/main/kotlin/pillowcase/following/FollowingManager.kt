package pillowcase.following

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.following.repository.FollowingRepository
import pillowcase.profile.Profile

@Component
class FollowingManager @Autowired constructor(
        private val repository: FollowingRepository) {

    fun follow(from: Profile, to : Profile) {
        if(!isFollower(from, to)) {
            repository.save(Follows(from, to))
        }
    }

    fun unfollow(from: Profile, to: Profile) {
        repository.deleteByFromIdAndToId(from.id!!, to.id!!)
    }

    fun isFollower(from: Profile, to: Profile): Boolean {
        return repository.existsByFromIdAndToId(from.id!!, to.id!!)
    }

    fun deleteAll(profile: Profile) {
        repository.deleteByToId(profile.id!!)
        repository.deleteByFromId(profile.id!!)
    }
}