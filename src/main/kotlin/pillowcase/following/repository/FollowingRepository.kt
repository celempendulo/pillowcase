package pillowcase.following.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.following.Follows

interface FollowingRepository : ArangoRepository<Follows, String> {

    fun existsByFromIdAndToId(from: String, to: String) : Boolean

    fun deleteByFromIdAndToId(from: String, to: String)

    fun deleteByFromId(id: String)

    fun deleteByToId(id: String)


}