package pillowcase.blocking

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import pillowcase.profile.Profile

@Edge("blocked")
class Blocked(@From val blocker: Profile, @To val blocked: Profile, @Id var id: String? = null)