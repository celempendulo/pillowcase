package pillowcase.blocking

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.blocking.repository.BlockedRepository
import pillowcase.profile.Profile

@Component
class BlockingManager @Autowired constructor(
        private val repository: BlockedRepository) {

    fun block(blocker: Profile, profile: Profile) {
        if(!repository.existsByBlockerIdAndBlockedId(blocker.id!!, profile.id!!)) {
            repository.save(Blocked(blocker, profile))
        }
    }

    fun unblock(blocker: Profile, blocked: Profile) {
        repository.findByBlockerIdAndBlockedId(blocker.id!!, blocked.id!!).ifPresent {
            repository.delete(it)
        }
    }

    fun isBlocked(blocker: Profile, blocked: Profile): Boolean {
        return repository.existsByBlockerIdAndBlockedId(blocker.id!!, blocked.id!!)
    }

    fun deleteAll(profile: Profile) {
        repository.deleteByBlockerId(profile.id!!)
        repository.deleteByBlockedId(profile.id!!)
    }
}