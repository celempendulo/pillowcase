package pillowcase.blocking.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.blocking.Blocked
import java.util.*

interface BlockedRepository : ArangoRepository<Blocked, String> {

    fun findByBlockerIdAndBlockedId(firstId: String, secondId: String): Optional<Blocked>

    fun existsByBlockerIdAndBlockedId(firstId: String, secondId: String): Boolean

    fun deleteByBlockerId(id: String)

    fun deleteByBlockedId(id: String)
}