package pillowcase.notification

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.api.service.NotificationService

@Component
class NotificationManager @Autowired constructor(
        private val service: NotificationService) {


    fun send(account: Account, notification: Any) {
        if (!service.send(account.id!!, notification)) {
            /***
             * SAVE TO DATABASE
             */
        }
    }


}