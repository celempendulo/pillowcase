package pillowcase.notification.reactions.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.notification.reactions.ReactionNotification

interface ReactionNotificationRepository : ArangoRepository<ReactionNotification, String> {

}