package pillowcase.notification.reactions

import com.arangodb.springframework.annotation.Document
import pillowcase.post.Post
import org.springframework.data.annotation.Id
import pillowcase.interactions.ReactionType
import java.time.LocalDateTime

@Document("reactionNotifications")
class ReactionNotification(
        val post: Post,
        val type: ReactionType,
        val number: Int,
        val names: List<String>,
        var read: Boolean = false,
        val sent: Boolean = false,
        val createdAt: LocalDateTime = LocalDateTime.now(),
        @Id var id: String? = null)