package pillowcase.reports

import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import pillowcase.post.Post
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

class Report(@From val account: Account,
             @To val post: Post,
             val reason: Reason,
             val createdAt: LocalDateTime= LocalDateTime.now(),
             @Id var id: String?=null) {

    enum class Reason{
        SPAM,
        ABUSIVE,
        DUPLICATE
    }
}