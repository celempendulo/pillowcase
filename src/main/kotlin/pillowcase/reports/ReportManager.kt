package pillowcase.reports

import epsilon.accounts.Account
import pillowcase.post.Post
import pillowcase.reports.repository.ReportRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ReportManager @Autowired constructor(
        private val reportRepository: ReportRepository) {

    fun add(account: Account, post: Post, reason: Report.Reason) {

        reportRepository.findByAccountAndPost(account, post).ifPresentOrElse({
            // Ignore this is a duplicate
        },{
            reportRepository.save(Report(account, post, reason))
        })
    }
}