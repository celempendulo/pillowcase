package pillowcase.reports.repository

import com.arangodb.springframework.repository.ArangoRepository
import epsilon.accounts.Account
import pillowcase.post.Post
import pillowcase.reports.Report
import java.util.*

interface ReportRepository: ArangoRepository<Report, String> {

    fun findByAccountAndPost(account: Account, post: Post): Optional<Report>
}