package pillowcase.search

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.edges.repository.ClusterNodeRepository
import pillowcase.edges.repository.ClusterPostInteractionRepository
import pillowcase.interactions.repository.PostInteractionRepository
import pillowcase.interactions.repository.ViewRepository
import pillowcase.post.Post
import pillowcase.post.Search
import pillowcase.post.repository.PostRepository
import pillowcase.visitor.Visitor


@Component
class SearchManager @Autowired constructor(
        private val postRepository: PostRepository,
        private val clusterPostInteractionRepository: ClusterPostInteractionRepository,
        private val clusterNodeRepository: ClusterNodeRepository,
        private val postInteractionRepository: PostInteractionRepository,
        private val viewRepository: ViewRepository,
        private val accountManager: AccountManager) {


    fun defaultSearch(account: Account, limit: Int): List<Post> {
        clusterNodeRepository.findByAccountId(account.id!!).let { node ->
            if (node.isEmpty) {
                return defaultSearch(limit)
            }
            val cluster = node.get().cluster!!
            var posts = postRepository.find(limit).toMutableList()
            posts.removeIf { post ->
                postInteractionRepository.findByPostIdAndAccountId(post.id!!, account.id!!).isPresent}
            return posts.sortedBy { post ->
                var count = 0.0
                clusterPostInteractionRepository.findByPostIdAndClusterId(post.id!!, cluster.id!!).ifPresent {
                    count += it.score()
                }
                count += post.score()
                count
            }.take(limit)
        }
    }

    fun defaultSearch(limit: Int): List<Post> {
        val posts = postRepository.find(limit).toMutableList()
        return posts.sortedBy { post ->
            post.score()
        }.take(limit)
    }

    fun defaultSearch(visitor: Visitor, limit: Int): List<Post> {
        val posts = postRepository.find(limit).toMutableList()
        posts.removeIf { post ->
            viewRepository.findByPostIdAndVisitorId(post.id!!, visitor.id!!).isPresent}
        return posts.sortedBy { post ->
            post.score()
        }.take(limit)
    }

    fun search(searcher: Any, search: Search): List<Post> {
        var posts :List<Post> = listOf()
        when (search.type) {
            Search.Type.USER -> {
                accountManager.getByHandler(search.data).ifPresent {
                    posts = searchByUser(it, search.skip, search.size)
                }
            }
            Search.Type.CAPTION -> {
                posts = searchByCaption(search.data, search.skip, search.size)
            }
            Search.Type.BLANK -> {
                if(searcher is Visitor)
                    posts = defaultSearch(searcher, search.size)
                else if(searcher is Account)
                    posts = defaultSearch(searcher, search.size)
            }
        }
        return posts.toList()
    }

    fun searchByUser(account: Account, skip: Int, limit:Int): List<Post>{
        val posts = postRepository.findByOwnerId(account.id!!)
        if(skip >= posts.size){
            return listOf()
        }
        return posts.subList(skip, posts.size)
    }

    fun searchByCaption(category: String, skip: Int, limit: Int): List<Post> {
        TODO("fix this")
        return postRepository.findByCategoryAndTopic(category,"", skip, limit).toList()
    }
}