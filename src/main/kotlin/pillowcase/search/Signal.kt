package pillowcase.search

enum class Signal {
    POPULAR,
    NEW,
    LIKED_BY,
    WHICH_TOPIC
}