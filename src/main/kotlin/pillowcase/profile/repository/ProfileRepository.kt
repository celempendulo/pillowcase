package pillowcase.profile.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.profile.Profile
import java.util.*

interface ProfileRepository: ArangoRepository<Profile, String> {

    fun findByAccountId(id: String): Optional<Profile>

    fun deleteByAccountId(id: String)

    fun existsBy(id: String): Boolean
}