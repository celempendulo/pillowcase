package pillowcase.profile.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.profile.Profile
import pillowcase.profile.ProfileOwner

interface ProfileOwnerRepository: ArangoRepository<ProfileOwner, String> {
}