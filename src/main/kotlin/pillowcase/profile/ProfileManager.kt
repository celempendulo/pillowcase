package pillowcase.profile

import epsilon.accounts.Account
import epsilon.failure.ClientError
import epsilon.localization.Messages
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.localization.PillowcaseTags
import pillowcase.profile.repository.ProfileOwnerRepository
import pillowcase.profile.repository.ProfileRepository

@Component
class ProfileManager @Autowired constructor(
        private val repository: ProfileRepository,
        private val profileOwnerRepository: ProfileOwnerRepository,
        private val messages: Messages) {

    fun getByAccount(accountId: String): Profile {
        repository.findByAccountId(accountId).let {
            if(it.isEmpty) throw ClientError(messages[PillowcaseTags.Error.PROFILE_NOT_FOUND])
            return it.get()
        }
    }

    fun get(id: String): Profile {
        repository.findById(id).let {
            if (it.isEmpty) throw ClientError(messages[PillowcaseTags.Error.PROFILE_NOT_FOUND])
            return it.get()
        }
    }

    fun add(account: Account): Profile {
        repository.findByAccountId(account.id!!).let {
            if (it.isPresent) return it.get()
            val profile = repository.save(Profile(account))
            val owner = ProfileOwner(profile = profile, owner = profile.account)
            profileOwnerRepository.save(owner)
            return profile;
        }
    }

    fun delete(account: Account) {
        repository.deleteByAccountId(account.id!!)
    }
}