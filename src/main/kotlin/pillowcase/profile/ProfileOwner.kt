package pillowcase.profile

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id

@Edge("profileOwner")
class ProfileOwner(@From val owner:Account, @To val profile: Profile, @Id var id: String?=null)