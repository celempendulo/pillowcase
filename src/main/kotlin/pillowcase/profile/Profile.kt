package pillowcase.profile

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Relations
import epsilon.accounts.Account
import epsilon.localization.Language
import org.springframework.data.annotation.Id

@Document("profiles")
class Profile(@Relations(edges = [ProfileOwner::class])val account: Account,
              var writingLanguage: Language = Language.ENGLISH, @Id var id:String? = null)