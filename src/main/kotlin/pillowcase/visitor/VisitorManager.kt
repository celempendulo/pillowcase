package pillowcase.visitor

import pillowcase.visitor.repository.VisitorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

@Component
class VisitorManager @Autowired constructor(
        private val repository: VisitorRepository) {

    fun create(): Visitor {
        return repository.save(Visitor())
    }


    fun get(id: String): Optional<Visitor> {
        return repository.findById(id)
    }

    fun createIfAbsent(id: String): Visitor {
        val container = get(id)
        if(container.isPresent) {
            return container.get()
        }
        return repository.save(Visitor(id=id))
    }

    fun delete(id: String) {
        repository.deleteById(id)
    }

    fun clean() {
        repository.deleteAll()
    }
}