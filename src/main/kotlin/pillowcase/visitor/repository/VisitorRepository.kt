package pillowcase.visitor.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.visitor.Visitor

interface VisitorRepository : ArangoRepository<Visitor, String>