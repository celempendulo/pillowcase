package pillowcase.visitor

import com.arangodb.springframework.annotation.ArangoId
import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

@Document("visitors")
data class Visitor(
        var createdOn: LocalDateTime = LocalDateTime.now(),
        var lastLogin: LocalDateTime = LocalDateTime.now(),
        @ArangoId var id: String?=null)