package pillowcase.interactions

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.interactions.repository.PostInteractionRepository
import pillowcase.interactions.repository.ViewRepository
import pillowcase.post.Post
import pillowcase.post.PostManager
import pillowcase.visitor.Visitor

@Component
class InteractionManager @Autowired constructor(
        private val repositoryPost: PostInteractionRepository,
        private val viewRepository: ViewRepository,
        private val clusterInteractionManager: ClusterInteractionManager,
        private val postManager: PostManager) {


    fun react(post: Post, type: ReactionType, account: Account) {
        repositoryPost.findByPostIdAndAccountId(post.id!!, account.id!!).let {
            val interaction = if(it.isEmpty) PostInteraction(account, post) else it.get()
            var isNew = false
            if(interaction.reaction == null) {
                interaction.reaction = type
                isNew = true
            }
            else {
                var number = post.reactions.values[interaction.reaction!!]!!
                post.reactions.values[interaction.reaction!!] = (--number).coerceAtLeast(0)
                post.reactions.total = (--post.reactions.total).coerceAtLeast(0)
                interaction.reaction = type
            }
            /**
             * Reaction manager should in general not have ability to modify a post in the database, but in this
             * post contains information it shouldn't contain (reactions to be precise)so it makes sense to give
             * reaction manager ability to update that information directly.
             */
            post.reactions.values[type]= post.reactions.values[type]!!+1
            post.reactions.total ++
            postManager.update(post)
            repositoryPost.save(interaction)
            if(isNew) {
                clusterInteractionManager.addReaction(account, post)
            }
        }
    }

    fun unreact(post: Post, account: Account) {
        repositoryPost.findByPostIdAndAccountId(post.id!!, account.id!!).ifPresent {
            var number = post.reactions.values[it.reaction]!!
            post.reactions.values[it.reaction!!] = (--number).coerceAtLeast(0)
            post.reactions.total = (post.reactions.total--).coerceAtLeast(0)
            postManager.update(post)
            clusterInteractionManager.removeReaction(account, post)
        }
    }

    fun view(post: Post, account: Account) {
        repositoryPost.findByPostIdAndAccountId(post.id!!, account.id!!).ifPresentOrElse({
            /** If you interacted with a post then you saw it **/
        }, {
            repositoryPost.save(PostInteraction(account, post))
            post.reactions.views++
            postManager.update(post)
            clusterInteractionManager.addView(account, post)
        })
    }

    fun view(post: Post, visitor: Visitor) {
        viewRepository.findByPostIdAndVisitorId(post.id!!, visitor.id!!).ifPresentOrElse({
            /** Nothing to do as person already stated they saw this post **/
        },{
            viewRepository.save(View(visitor, post))
        })
    }

    fun comment(post: Post, comment: PostInteraction.Comment, account: Account) {
        if(comment.audio != null) {
            /**
             * TODO("add audio file for the comment")
             */
        }
        else {
            repositoryPost.findByPostIdAndAccountId(post.id!!, account.id!!).ifPresentOrElse({
                it.comments.add(comment)
                repositoryPost.save(it)
            },{
                val interaction = PostInteraction(account, post)
                interaction.comments.add(comment)
                repositoryPost.save(interaction)
            })
        }
        clusterInteractionManager.addComment(account, post)
    }


    fun getReactors(post: Post): Post.Reactions {
        val reactions = Post.Reactions()
        repositoryPost.getAllWithReactions(post.id!!).forEach {
            reactions.values[it.reaction!!] = reactions.values[it.reaction!!]!!+1
            reactions.total ++
        }
        return reactions
    }

    fun getComments(post: Post): MutableList<PostInteraction.Comment> {
        val comments = mutableListOf<PostInteraction.Comment>()
        repositoryPost.getAllWithComments(post.id!!).forEach {
            comments.addAll(it.comments)
        }
        comments.sort()
        return comments
    }

}