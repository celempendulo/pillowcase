package pillowcase.interactions

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import pillowcase.post.Post
import pillowcase.visitor.Visitor

@Edge("views")
class View(@From val visitor: Visitor, @To val post: Post, @Id var id: String?=null)