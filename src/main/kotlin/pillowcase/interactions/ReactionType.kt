package pillowcase.interactions

 enum class ReactionType {
        Upvote,
        Downvote
 }