package pillowcase.interactions

import epsilon.accounts.Account
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.edges.ClusterNode
import pillowcase.edges.ClusterPostInteraction
import pillowcase.edges.repository.ClusterNodeRepository
import pillowcase.edges.repository.ClusterPostInteractionRepository
import pillowcase.post.Post
import java.util.*
import java.util.function.BiConsumer

@Component
class ClusterInteractionManager @Autowired constructor(
        private val interactions: ClusterPostInteractionRepository,
        private val clusterNodes: ClusterNodeRepository) {

    fun addReaction(account: Account, post:Post) {

        doAction(account, post, BiConsumer { container, node ->
            if(container.isPresent) {
                container.get().reactions++
                interactions.save(container.get())
            }
            else {
                val interaction = ClusterPostInteraction(node.cluster!!, post)
                interaction.reactions++
                interactions.save(interaction)
            }
        })
    }

    fun addView(account: Account, post:Post) {
        doAction(account, post, BiConsumer { container, node ->
            if(container.isPresent) {
                container.get().views++
                interactions.save(container.get())
            }
            else {
                val interaction = ClusterPostInteraction(node.cluster!!, post)
                interaction.views++
                interactions.save(interaction)
            }
        })
    }

    fun addComment(account: Account, post:Post) {
        doAction(account, post, BiConsumer { container, node ->
            if(container.isPresent) {
                container.get().comments++
                interactions.save(container.get())
            }
            else {
                val interaction = ClusterPostInteraction(node.cluster!!, post)
                interaction.comments++
                interactions.save(interaction)
            }
        })
    }

    fun removeReaction(account: Account, post:Post) {

        doAction(account, post, BiConsumer { container, node ->
            if(container.isPresent) {
                if(container.get().reactions > 0) {
                    container.get().reactions--
                    interactions.save(container.get())
                }
            }
        })
    }

    private fun doAction(account: Account, post:Post, action: BiConsumer<Optional<ClusterPostInteraction>, ClusterNode>) {
        clusterNodes.findByAccountId(account.id!!).ifPresentOrElse({ clusterNode ->
            interactions.findByPostIdAndClusterId(post.id!!, account.id!!).let {
                action.accept(it, clusterNode)
            }
        },{})
    }
}