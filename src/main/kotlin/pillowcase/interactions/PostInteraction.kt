package pillowcase.interactions

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id
import org.springframework.web.multipart.MultipartFile
import pillowcase.post.Post
import java.time.LocalDateTime
import java.util.*

@Edge("interactions")
class PostInteraction(@From val account: Account, @To val post: Post, var seen: Boolean=true,
                  var reaction: ReactionType? = null, var comments: MutableList<Comment> = mutableListOf(),
                  @Id var id: String? = null) {

    class Comment(var text: String, var audio: MultipartFile? = null, val id: String = UUID.randomUUID().toString(),
                  val createdAt: LocalDateTime = LocalDateTime.now()): Comparable<Comment> {

        override fun compareTo(other: Comment): Int {
            return this.createdAt.compareTo(other.createdAt)
        }

    }
}