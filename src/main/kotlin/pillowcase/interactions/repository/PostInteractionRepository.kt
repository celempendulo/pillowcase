package pillowcase.interactions.repository

import com.arangodb.ArangoCursor
import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import org.springframework.data.repository.query.Param
import pillowcase.interactions.PostInteraction
import java.util.*

interface PostInteractionRepository : ArangoRepository<PostInteraction, String> {

    fun findByPostIdAndAccountId(postId: String, accountId: String): Optional<PostInteraction>

    @Query("""FOR interaction IN 1..1 INBOUND @post @@#collection 
                    FILTER interaction.reaction != null
                    return interaction""")
    fun getAllWithReactions(@Param("post") id: String): ArangoCursor<PostInteraction>

    @Query("""FOR interaction IN 1..1 INBOUND @post @@#collection 
                    FILTER interaction.comments != []
                    return interaction""")
    fun getAllWithComments(@Param("post")id: String): ArangoCursor<PostInteraction>

}