package pillowcase.interactions.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.interactions.View
import java.util.*

interface ViewRepository : ArangoRepository<View, String> {

     fun findByPostIdAndVisitorId(postId: String, visitorId: String): Optional<View>


}