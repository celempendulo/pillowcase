package pillowcase.localization

class PillowcaseTags {
    object Error {
        const val PROFILE_NOT_FOUND = "error.profile.notFound"

        const val AUDIO_TOO_LARGE = "error.audio.tooLarge"

        const val REACTION_NOT_FOUND = "error.reaction.notFound"
        const val TOPIC_ALREADY_EXIST = "error.topic.nameTaken"
        const val TOPIC_NOT_FOUND = "error.topic.notFound"

        const val POST_NOT_OWNER = "error.post.notOwner"
        const val POST_NOT_FOUND = "error.post.notFound"
        const val POST_AUDIO_DUPLICATE = "error.post.audioDuplicate"
    }
}