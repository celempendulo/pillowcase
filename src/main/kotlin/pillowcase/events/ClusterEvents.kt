package pillowcase.events

import com.arangodb.springframework.core.mapping.event.AbstractArangoEventListener
import com.arangodb.springframework.core.mapping.event.BeforeDeleteEvent
import org.springframework.stereotype.Component
import pillowcase.edges.repository.ClusterNodeRepository
import pillowcase.ml.CustomCluster

@Component
class ClusterEvents constructor(
        private val nodeRepository: ClusterNodeRepository): AbstractArangoEventListener<CustomCluster>(
) {

    override fun onBeforeDelete(event: BeforeDeleteEvent<CustomCluster>) {
        nodeRepository.deleteByClusterId(event.source as String)
    }
}