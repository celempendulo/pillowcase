package pillowcase.events

import com.arangodb.springframework.core.mapping.event.AfterSaveEvent
import com.arangodb.springframework.core.mapping.event.BeforeDeleteEvent
import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import epsilon.events.AAccountEvents
import epsilon.tokens.TokenManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.post.PostManager
import pillowcase.profile.ProfileManager

@Component
class AccountEvents @Autowired constructor(
        private val tokenManager: TokenManager,
        private val accountManager: AccountManager,
        private val profileManager: ProfileManager,
        private val postManager: PostManager) : AAccountEvents(tokenManager, accountManager) {

    override fun onAfterSave(event: AfterSaveEvent<Account>?) {
        profileManager.add(event!!.source)
        super.onAfterSave(event)
    }

    override fun onBeforeDelete(event: BeforeDeleteEvent<Account>) {
        accountManager.get(event.source as String).ifPresent {
            profileManager.delete(it)
            postManager.deleteAll(it)
        }
        super.onBeforeDelete(event)
    }
}