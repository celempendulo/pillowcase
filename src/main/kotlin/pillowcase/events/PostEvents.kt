package pillowcase.events

import com.arangodb.springframework.core.mapping.event.AbstractArangoEventListener
import com.arangodb.springframework.core.mapping.event.AfterSaveEvent
import com.arangodb.springframework.core.mapping.event.BeforeDeleteEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pillowcase.edges.PostOwner
import pillowcase.edges.repository.PostOwnerRepository
import pillowcase.post.Post
import pillowcase.post.PostManager
import pillowcase.post.saved.SavedPostManager

@Component
class PostEvents @Autowired constructor(
        private val ownershipRepo: PostOwnerRepository,
        private val postManager: PostManager,
        private val savedPostManager: SavedPostManager): AbstractArangoEventListener<Post>() {

    override fun onAfterSave(event: AfterSaveEvent<Post>) {
        /**
         * Connect post with owner
         */
        ownershipRepo.findByPostId(event.source.id!!).ifPresentOrElse({},{
            ownershipRepo.save(PostOwner(owner = event.source.owner, post = event.source))
        })
    }

    override fun onBeforeDelete(event: BeforeDeleteEvent<Post>) {
        postManager.get(event.source as String).ifPresent {
            ownershipRepo.deleteByPostId(it.id!!)
            savedPostManager.deleteAll(it)
        }


    }
}