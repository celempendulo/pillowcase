package pillowcase.events

import com.arangodb.springframework.core.mapping.event.AbstractArangoEventListener
import com.arangodb.springframework.core.mapping.event.AfterSaveEvent
import com.arangodb.springframework.core.mapping.event.BeforeDeleteEvent
import org.springframework.stereotype.Component
import pillowcase.blocking.BlockingManager
import pillowcase.following.FollowingManager
import pillowcase.post.saved.SavedPostManager
import pillowcase.profile.Profile
import pillowcase.profile.ProfileManager

@Component
class ProfileEvents constructor(
        private val profileManager: ProfileManager,
        private val blockingManager: BlockingManager,
        private val followingManager: FollowingManager,
        private val savedPostManager: SavedPostManager) : AbstractArangoEventListener<Profile>() {

    override fun onBeforeDelete(event: BeforeDeleteEvent<Profile>) {
        val profile = profileManager.get(event.source as String)
        blockingManager.deleteAll(profile)
        followingManager.deleteAll(profile)
        savedPostManager.deleteAll(profile)
    }
}