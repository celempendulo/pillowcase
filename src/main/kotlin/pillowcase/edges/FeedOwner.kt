package pillowcase.edges

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id
import pillowcase.feed.Feed

@Edge("feedOwner")
class FeedOwner(@From val account: Account, @To val feed: Feed, @Id var id: String?=null)