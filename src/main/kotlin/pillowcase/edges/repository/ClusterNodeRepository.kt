package pillowcase.edges.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.edges.ClusterNode
import java.util.*

interface ClusterNodeRepository: ArangoRepository<ClusterNode, String> {

    fun deleteByClusterId(id: String)

    fun findByAccountId(id: String): Optional<ClusterNode>
}