package pillowcase.edges.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.edges.ClusterPostInteraction
import java.util.*

interface ClusterPostInteractionRepository: ArangoRepository<ClusterPostInteraction, String> {

    fun findByPostIdAndClusterId(postId: String, clusterId: String): Optional<ClusterPostInteraction>
}