package pillowcase.edges.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.edges.FeedOwner

interface FeedOwnerRepository: ArangoRepository<FeedOwner, String> {
}