package pillowcase.edges.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.edges.PostOwner
import java.util.*

interface PostOwnerRepository : ArangoRepository<PostOwner, String> {

    fun findByPostId(id: String): Optional<PostOwner>
    fun deleteByPostId(id: String)
}