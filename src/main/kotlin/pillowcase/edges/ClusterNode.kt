package pillowcase.edges

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import epsilon.data.Dimension
import epsilon.data.IClusterNode
import pillowcase.ml.CustomCluster
import pillowcase.ml.CustomDimension
import java.time.LocalDate
import java.util.*

@Edge("clusterNode")
class ClusterNode(@From val account: Account, @To var cluster: CustomCluster? = null) : IClusterNode {
    private val values: MutableMap<Dimension, Any> = mutableMapOf()

    init {
        val calender = Calendar.getInstance()
            calender.time = account.dob!!
            values[CustomDimension.AGE] = LocalDate.now().year - calender.get(Calendar.YEAR)
            values[CustomDimension.GENDER] = account.gender!!
    }

    fun clone(): IClusterNode {
        return ClusterNode(account)
    }

    override fun getValues(): MutableMap<Dimension, Any> {
        return values
    }
}