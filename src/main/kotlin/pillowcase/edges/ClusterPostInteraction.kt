package pillowcase.edges

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import pillowcase.ml.CustomCluster
import pillowcase.post.Post

@Edge("clusterPostInteraction")
class ClusterPostInteraction(@From val cluster: CustomCluster, @To val post: Post,
                             var reactions:Int = 0, var comments:Int = 0, var views: Int = 0,
                             @Id var id: String? = null) {


    fun score(): Double {
        var score = 0.0

        if(views > 0) {
            score += (reactions + comments)/views
        }
        return score
    }
}