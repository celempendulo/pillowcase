package pillowcase.edges

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import epsilon.accounts.Account
import org.springframework.data.annotation.Id
import pillowcase.post.Post

@Edge("postOwner")
data class PostOwner(@From val post: Post, @To val owner: Account, @Id var id:String?=null)