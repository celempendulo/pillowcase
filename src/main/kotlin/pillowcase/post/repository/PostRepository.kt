package pillowcase.post.repository

import com.arangodb.ArangoCursor
import com.arangodb.springframework.annotation.Query
import com.arangodb.springframework.repository.ArangoRepository
import epsilon.accounts.Account
import pillowcase.post.Post
import org.springframework.data.repository.query.Param
import java.util.*

interface PostRepository : ArangoRepository<Post, String>{

    fun findByChecksum(checksum: String): Optional<Post>

    fun findByOwnerId(id: String): List<Post>

    @Query("FOR doc IN #collection SORT RAND() LIMIT @limit RETURN doc")
    fun find(@Param("limit") limit: Int): ArangoCursor<Post>

    fun findByCategoryAndTopic(@Param("caption")caption: String, @Param("topic")about: String,
                               @Param("offset") offset:Int, @Param("limit")limit: Int): ArangoCursor<Post>
}