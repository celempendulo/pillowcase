package pillowcase.post

import com.arangodb.springframework.annotation.Document
import com.arangodb.springframework.annotation.Relations
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import epsilon.accounts.Account
import epsilon.localization.Language
import org.springframework.data.annotation.Id

import pillowcase.edges.PostOwner
import pillowcase.interactions.ReactionType
import java.time.LocalDateTime

@JsonIgnoreProperties("file","owner")
@Document("posts")
class Post(
        var category: String,
        var topic: String,
        var reactions: Reactions = Reactions(),
        var language: Language,
        @Relations(edges = [PostOwner::class]) var owner: Account,
        val createdAt: LocalDateTime = LocalDateTime.now(),
        var checksum: String = "",
        @Id var id: String? = null) {

    override fun equals(other: Any?): Boolean {
        if(other is Post) {
            if(id != null)
                return id == other.id
        }
        return false
    }

    fun score(): Double {
        var count = 0.0
        if(reactions.views > 0) {
           count+= reactions.values.map { it.value }.sum().toDouble()/reactions.views
        }
        return count
    }

    class Reactions(val values: MutableMap<ReactionType, Int> = mutableMapOf(), var total:Int = 0, var views:Int = 0) {
        init {
            values.ifEmpty {
                ReactionType.values().forEach {
                    values[it] = 0
                }
            }
        }
    }

}