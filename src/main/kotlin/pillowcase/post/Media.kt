package pillowcase.post

import epsilon.media.MediaFormat
import org.springframework.web.multipart.MultipartFile

class Media(val format: MediaFormat,val  data: MultipartFile)