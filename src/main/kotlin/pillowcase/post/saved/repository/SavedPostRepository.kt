package pillowcase.post.saved.repository

import com.arangodb.springframework.repository.ArangoRepository
import pillowcase.post.saved.SavedPost

interface SavedPostRepository: ArangoRepository<SavedPost, String> {


    fun existsByProfileIdAndPostId(firstId: String, secondId: String): Boolean

    fun deleteByProfileIdAndPostId(firstId: String, secondId: String)

    fun deleteByProfileId(id: String)

    fun deleteByPostId(id: String)
}