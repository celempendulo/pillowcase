package pillowcase.post.saved

import com.arangodb.springframework.annotation.Edge
import com.arangodb.springframework.annotation.From
import com.arangodb.springframework.annotation.To
import org.springframework.data.annotation.Id
import pillowcase.post.Post
import pillowcase.profile.Profile

@Edge("savedPost")
class SavedPost(@From val profile: Profile, @To val post: Post, @Id var id: String? = null)