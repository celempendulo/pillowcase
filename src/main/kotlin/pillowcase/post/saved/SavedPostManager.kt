package pillowcase.post.saved

import org.springframework.stereotype.Component
import pillowcase.post.Post
import pillowcase.post.saved.repository.SavedPostRepository
import pillowcase.profile.Profile

@Component
class SavedPostManager constructor(
        private val repository: SavedPostRepository) {

    fun add(profile: Profile, post: Post) {
        if(!repository.existsByProfileIdAndPostId(profile.id!!, post.id!!)) {
            repository.save(SavedPost(profile, post))
        }
    }

    fun delete(profile: Profile, post: Post) {
        repository.deleteByProfileIdAndPostId(profile.id!!, post.id!!)
    }

    fun deleteAll(profile: Profile) {
        repository.deleteByProfileId(profile.id!!)
    }

    fun deleteAll(post: Post) {
        repository.deleteByPostId(post.id!!)
    }
}