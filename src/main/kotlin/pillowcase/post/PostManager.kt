package pillowcase.post

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import epsilon.ApplicationProperties
import epsilon.accounts.Account
import epsilon.failure.ClientError
import epsilon.localization.Messages
import epsilon.localization.Tag
import epsilon.media.MediaManager
import epsilon.media.MediaProperties
import pillowcase.post.repository.PostRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import pillowcase.localization.PillowcaseTags
import java.util.*

@Component
@EnableConfigurationProperties(ApplicationProperties::class, MediaProperties::class)
class PostManager @Autowired constructor(
        private val mediaProperties: MediaProperties,
        private val repository: PostRepository,
        private val messages: Messages,
        private val mediaManager: MediaManager,
        private val amazonClient : AmazonS3Client){

    @Value("moods.s3.bucketName")
    private lateinit var bucketName: String

    fun create(post: Post, audio: Media): Post {

        if(mediaManager.getSize(audio.data) > mediaProperties.maxSize) {
            throw ClientError(messages[PillowcaseTags.Error.AUDIO_TOO_LARGE])
        }
        val stream = if(audio.format == mediaProperties.audioFormat) audio.data.inputStream else
            mediaManager.convert(audio.data.inputStream, audio.format)
        post.checksum = mediaManager.getMD5(stream)
        val result = repository.save(post)
        val request = PutObjectRequest(bucketName,
                "${result.id}.${mediaProperties.audioFormat.name.toLowerCase()}",
                stream,
                ObjectMetadata())
                .withCannedAcl(CannedAccessControlList.PublicRead)
        amazonClient.putObject(request)
        return result
    }

    fun deleteAll(account: Account) {
        repository.findByOwnerId(account.id!!).forEach {
            repository.delete(it)
        }
    }

    fun delete(account: Account, id: String): Post {
        val post = forceGet(id)
        if(account.roles.contains(Account.Roles.ADMIN) ||
                account.roles.contains(Account.Roles.MAINTAINER) ) {
            deleteAudio(post.id!!)
            repository.delete(post)
            return post
        }
        if (account.id == post.owner.id!!) {
            deleteAudio(post.id!!)
            repository.delete(post)
            return post
        }
        else {
            throw ClientError(messages[Tag.Error.PERMISSION_DENIED])
        }
    }

    fun update(requester: Account, id: String, category: String?, topic: String?): Post {
        val post = forceGet(id)
        if(post.owner.id == requester.id) {
            if (topic != null) {
                post.topic = topic
            }
            if (category != null) {
                post.category = category
            }
            return repository.save(post)
        }
        else {
            throw ClientError(messages[Tag.Error.PERMISSION_DENIED])
        }
    }

    /**
     * This method is intended to be used ONLY by reaction manager to update post after modifying reactions
     */
    fun update(post: Post) {
        repository.save(post)
    }


    private fun deleteAudio(postId:String) {
        amazonClient.deleteObject(DeleteObjectRequest(bucketName, "$postId.mp3"))
    }

    fun get(ids: List<String>): List<Post> {
        val posts = mutableListOf<Post>()
        ids.forEach {
            repository.findById(it).ifPresent {post ->
                posts.add(post)
            }
        }
        return posts
    }
    fun get(id:String): Optional<Post> {
        return repository.findById(id)
    }

    fun forceGet(id: String): Post {
        val container = get(id)
        if(container.isEmpty) {
            throw ClientError(messages[PillowcaseTags.Error.POST_NOT_FOUND])
        }
        return container.get()
    }

    fun getSize(): Long {
        return repository.count()
    }

    fun clean() {
        repository.findAll().forEach {
            repository.delete(it)
        }
    }

}