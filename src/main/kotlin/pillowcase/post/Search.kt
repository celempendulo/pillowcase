package pillowcase.post

data class Search(val type: Type = Type.BLANK,
                  val data:String = "",
                  val sort: Sort = Sort.LATEST,
                  val size:Int = 4,
                  val skip:Int = 0){

    enum class Type {
        BLANK,
        CAPTION,
        USER

    }

    enum class Sort {
        LATEST,
        OLDEST,
        MOST_POPULAR,
        LEAST_POPULAR
    }
}