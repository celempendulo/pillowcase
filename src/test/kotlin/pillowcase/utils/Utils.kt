package pillowcase.utils

object Utils {

    fun getVariables(link: String): MutableMap<String, String> {
        var values = link.substringAfter("?")
        val map = mutableMapOf<String, String>()
        values.split("&").forEach{
           val pair = it.split("=")
            map[pair[0]] = pair[1]
        }
        return map
    }
}