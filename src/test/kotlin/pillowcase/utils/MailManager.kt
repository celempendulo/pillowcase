package pillowcase.utils

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.File

@Component
class MailManager {

    @Value("\${pillowcase.mail.directory}")
    private lateinit var directory: String
    fun readLatsMail(): List<String> {
        val file = getLast()
        if(file !=null) {
            return file.readLines(Charsets.UTF_8)
        }
        return listOf()
    }

    fun clean(){
        val dir = File(directory)
        val files = dir.listFiles()
        if (files != null && files.isEmpty()) {
            files.forEach { it.delete() }
        }
    }

    private fun getLast(): File? {
        val dir = File(directory)
        val files = dir.listFiles()
        if (files == null || files.isEmpty()) {
            return null
        }
        var lastModifiedFile = files[0]
        for (i in 1 until files.size) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i]
            }
        }
        return lastModifiedFile
    }
}