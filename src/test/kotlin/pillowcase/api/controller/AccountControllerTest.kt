package pillowcase.api.controller

import epsilon.accounts.Account
import epsilon.configuration.Initializer
import epsilon.failure.ClientError
import epsilon.localization.Language
import org.junit.Assert
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import pillowcase.api.controller.AccountController.LoginRequest
import pillowcase.api.controller.AccountController.RegisterRequest
import pillowcase.api.controller.AccountController.UpdateRequest
import pillowcase.api.controller.AccountController.UpdateResponse
import pillowcase.api.controller.AccountController.ConfirmEmailRequest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.*
import org.springframework.security.authentication.BadCredentialsException
import pillowcase.AControllerTest
import pillowcase.api.Urls
import pillowcase.api.model.GeneralResponse
import pillowcase.security.SecurityUtils
import pillowcase.utils.Utils


@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountControllerTest @Autowired constructor(private val initializer: Initializer):
        AControllerTest() {

    @BeforeAll
    fun init() {
        initializer.init()
    }

    @Test
    fun register() {
        val request = RegisterRequest("first_user", "Password123", "PIC")
        ensureAccountIsAbsent(request.handler)
        val response = doSend(body = request, endPoint = Urls.REGISTRATION,
                clazz =  GeneralResponse::class.java).body!!
        Assert.assertTrue("Account not created [${response.message}]", response.success)
        val account = doSend(endPoint = Urls.MYSELF, method = HttpMethod.GET, clazz = Account::class.java).body!!
        /**
         * Confirm that everything is as expected
         */
        Assertions.assertEquals(request.handler, account.handler, "handler mismatch")
        Assertions.assertTrue(account.active, "account not active")
        Assertions.assertFalse(account.blocked, "account is blocked")
        Assertions.assertEquals(request.pp, account.pp, "pp is null")
        Assertions.assertNull(account.email, "email is not null")
        Assertions.assertFalse(account.emailVerified, "email is marked as verified")
        Assertions.assertNull(account.password, "password is not null")
        Assertions.assertTrue(accountManager.hasAccount(account.id!!),"account not found")
    }

    @Test
    fun register_handlerTaken() {
        val account = createAccount("random_user")
        val request = RegisterRequest(handler = account.handler!!, password = "Password123", pp="CEO")
        val response = doSend(body = request, endPoint = Urls.REGISTRATION, clazz = ClientError::class.java)
        Assertions.assertEquals("Handler is taken",response.body!!.message, "Incorrect error message")
    }

    @Test
    fun register_passwordInvalidFormat() {
        ensureAccountIsAbsent("random_user")
        val request = RegisterRequest("random_user","Pass","XYZ")
        val response = doSend(body = request, endPoint = Urls.REGISTRATION, clazz = ClientError::class.java)
        Assertions.assertEquals("Password does not meet requirements", response.body!!.message,
                "Incorrect error message")
    }

    @Test
    fun register_handlerInvalidFormat() {
        ensureAccountIsAbsent("random_user")
        val request = RegisterRequest("random user","Pass","XYZ")
        val response = doSend(body = request, endPoint = Urls.REGISTRATION, clazz = ClientError::class.java)
        Assertions.assertEquals("Invalid handler format", response.body!!.message,
                "Incorrect error message")
    }

    @Test
    fun login() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val account = doSend(endPoint = Urls.MYSELF, clazz = Account::class.java, method = HttpMethod.GET).body!!
        Assertions.assertEquals(login.handler, account.handler, "Incorrect data")
    }

    @Test
    fun login_BadCredentials() {
        val login = LoginRequest("random_user", "_Password123")
        doLogin(login, true)
        doSend(endPoint = Urls.LOGOUT, method = HttpMethod.GET, clazz = GeneralResponse::class.java)
        val request = LoginRequest(login.handler, "_${login.password}")
        val response = doSend(body = request, endPoint = Urls.LOGIN, clazz = BadCredentialsException::class.java).body!!

        Assertions.assertEquals("Incorrect username or password", response.message,
                "Incorrect error message")
    }

    @Test
    fun update() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val update = UpdateRequest(
                oldPassword = login.password,
                password = "RandomPassword12",
                pp = "Java",
                languages = listOf(Language.ZULU, Language.AFRIKAANS))

        val response = doSend(body = update, endPoint = Urls.UPDATE_ACCOUNT, clazz = GeneralResponse::class.java).body!!
        Assertions.assertTrue(response.success, "update not a success")

        accountManager.getByHandler(login.handler).get().let {
            Assertions.assertEquals(update.languages, it.languages, "languages don't match")
            Assertions.assertEquals(update.pp, it.pp, "pp don't match")
        }
    }

    @Test
    fun update_incorrectPassword() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val update = UpdateRequest(
                oldPassword = "_${login.password}",
                password = "RandomPassword12")

        val response = doSend(body = update, endPoint = Urls.UPDATE_ACCOUNT, clazz = ClientError::class.java).body!!
        Assertions.assertEquals("Incorrect password", response.message,
                "Incorrect error message")
    }

    @Test
    fun update_invalidEmail() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val response = doSend(body = """{"email":"randomlocalmail.com"}""", endPoint = Urls.UPDATE_EMAIL,
                clazz = ClientError::class.java).body!!

        Assertions.assertEquals("Invalid email format", response.message,"Incorrect error message")
    }

    @Test
    fun update_email() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val response = doSend(body="random@localmail.com", endPoint = Urls.UPDATE_EMAIL, method = HttpMethod.POST,
                clazz = GeneralResponse::class.java).body!!

        Assertions.assertTrue(response.success, "email not update")
        val links = mailManager.readLatsMail().filter { it.startsWith("http") }
        Assertions.assertFalse(links.isEmpty(), "No link was found")
        val values = Utils.getVariables(links[0])
        Assertions.assertNotNull(values["id"], "id was not found on link")
        Assertions.assertNotNull(values["code"], "code was not found on link")
    }

    @Test
    fun confirmEmail() {
        val login = LoginRequest("random_user", "Password123")
        doLogin(login, true)
        val response = doSend(body="random@localmail.com", endPoint = Urls.UPDATE_EMAIL, method = HttpMethod.POST,
                clazz = GeneralResponse::class.java).body!!

        val links = mailManager.readLatsMail().filter { it.startsWith("http") }
        val values = Utils.getVariables(links[0])
        Assertions.assertNotNull(values["id"], "id was not found on link")
        val code = values["code"]?.substringBefore("<")
        Assertions.assertNotNull(code, "code was not found on link")

        val confirm = ConfirmEmailRequest(id = values["id"]!!, code = code!!, email = "random@localmail.com")
        doSend(body = confirm, endPoint = Urls.CONFIRM_EMAIL, clazz = String::class.java)

        val account = accountManager.forceGet(values["id"]!!)
        Assertions.assertTrue(account.emailVerified, "email not verified")
    }

    @Test
    fun logout() {
        val login = LoginRequest("epsilon_", "Password123")
        doLogin(login, true)
        var account = doSend(endPoint = Urls.MYSELF, method = HttpMethod.GET,
                clazz = Account::class.java).body!!
        Assertions.assertEquals(login.handler, account.handler, "Incorrect username/handler")
        val response =  doSend(endPoint = Urls.LOGOUT, method = HttpMethod.GET,
                clazz = GeneralResponse::class.java).body!!
        Assert.assertTrue("Logout failed", response.success)
        val me = doSend(body="", endPoint = Urls.MY_ID, method = HttpMethod.GET, clazz = String::class.java)
        Assertions.assertTrue(me.body!!.startsWith("visitor"), "Incorrect username/handler")
    }

    @Test
    fun blacklistedToken() {

        val login = LoginRequest("epsilon_", "Password123")
        doLogin(login, true)
        val jwt = cookies.filter { it.startsWith(SecurityUtils.PillowcaseToken.Type.JWT.name) }[0]
        var account = doSend(endPoint = Urls.MYSELF, method = HttpMethod.GET,
                clazz = Account::class.java).body!!
        Assertions.assertEquals(login.handler, account.handler, "Incorrect username/handler")
        val response =  doSend(endPoint = Urls.LOGOUT, method = HttpMethod.GET,
                clazz = GeneralResponse::class.java).body!!
        Assert.assertTrue("Logout failed", response.success)
        var me = doSend(body="", endPoint = Urls.MY_ID, method = HttpMethod.GET, clazz = String::class.java)
        Assertions.assertTrue(me.body!!.startsWith("visitor"), "Incorrect username/handler")

        /**
         * Now put the blacklisted token back and check your id
         */
        cookies.add(jwt)
        me = doSend(body="", endPoint = Urls.MY_ID, method = HttpMethod.GET, clazz = String::class.java)
        println(me.body)
        Assertions.assertTrue(me.body!!.startsWith("visitor"), "Incorrect username/handler")

    }

    @AfterAll
    fun done() {
        super.clean()
    }


    @AfterEach
    fun afterTest() {
        cookies = mutableListOf()
        super.clean()
    }


    fun <T> doUpdate(loginRequest: LoginRequest, updateRequest: UpdateRequest,
                     clazz: Class<T>): ResponseEntity<T> {
        doLogin(loginRequest)
        return doSend(body = updateRequest, endPoint = "update", clazz = clazz)
    }

}