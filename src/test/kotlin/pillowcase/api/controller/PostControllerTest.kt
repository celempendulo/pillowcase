package pillowcase.api.controller

/***
import epsilon.failure.ClientError
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import pillowcase.AControllerTest
import pillowcase.api.controller.AccountController.LoginRequest
import pillowcase.api.controller.PostController.UpdateRequest
import pillowcase.post.Post
import pillowcase.post.PostManager
import pillowcase.post.Search

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostControllerTest @Autowired constructor(
        private val postManager: PostManager)
        : AControllerTest() {



    @Test
    fun post() {
        val login = LoginRequest("random", "Password12")
        doLogin(login, true)
        val post = Post(listOf("lock-down", "covid-21"), "We should be in lock-down forever")
        val response = doSend(body = post, endPoint = "post" , clazz = Post::class.java)
        Assertions.assertTrue(isPresent(response.body!!), "post not added")
    }

    @Test
    fun delete() {
        val login = LoginRequest("random", "Password12")
        doLogin(login, true)
        var post = Post(listOf("lock-down", "covid-21"), "We should be in lock-down forever")
        val response = doSend(body = post, endPoint = "post" , clazz = Post::class.java)
        post = response.body!!
        Assertions.assertTrue(isPresent(post), "post not added")

        doSend(post.id!!, endPoint = "post", method = HttpMethod.DELETE, clazz = Post::class.java)
        Assertions.assertFalse(isPresent(post), "post not deleted")
    }

    @Test
    fun delete_notOwner() {
        createAccount("random_user")
        accountManager.getByHandler("random_user").get().let { owner->
            val post = postManager.create(Post(owner = owner, tags = listOf("test","delete"),
                    caption = "Try to delete me"))
            doLogin(LoginRequest("chancer","Password123"), true)
            val response = doSend(post.id!!, endPoint = "post", method = HttpMethod.DELETE,
                    clazz = ClientError::class.java).body!!

            Assertions.assertEquals("Permission denied", response.message, "Incorrect error message")
            Assertions.assertTrue(isPresent(post), "post deleted")
        }
    }

    @Test
    fun update() {
        val login = LoginRequest("random", "Password12")
        doLogin(login, true)
        var post = Post(listOf("lock-down", "covid-21"), "We should be in lock-down forever")
        val response = doSend(body = post, endPoint = "post" , clazz = Post::class.java)
        post = response.body!!

        val update = UpdateRequest(post.id!!, "I was joking i hate lock-down")
        doSend(update, endPoint = "post", method = HttpMethod.PATCH, clazz = Post::class.java)
        postManager.forceGet(post.id!!).let {
            Assertions.assertEquals(update.caption, it.caption, "Caption not changed")
        }
    }

    @Test
    fun update_notOwner() {
        createAccount("random_user")
        accountManager.getByHandler("random_user").get().let { owner->
            val post = postManager.create(Post(owner= owner , tags = listOf("test","update"), caption = "Try to update me"))
            doLogin(LoginRequest("chancer","Password123"), true)

            val update = UpdateRequest(post.id!!, "I was joking i hate lock-down")
            val response = doSend(update, endPoint = "post", method = HttpMethod.PATCH,
                    clazz = ClientError::class.java).body!!

            Assertions.assertEquals("Permission denied", response.message, "Incorrect error message")
            postManager.forceGet(post.id!!).let {
                Assertions.assertEquals(post.caption, it.caption, "Caption changed")
            }
        }
    }

    @Test
    fun search() {
        val firstPosts = listOf(
                Post(listOf("java","code"),"Java is dead"),
                Post(listOf("kotlin", "code"), "Kotlin is alive"),
                Post(listOf("kotlin", "code"), "Kotlin is alive")
        )
        addPost("random_user", firstPosts)

        val secondPosts = listOf(
                Post(listOf("covid"),"Covid will be back in 2015"),
                Post(listOf("covid"), "covid has nothing to do with 5G"),
                Post(listOf("covid"), "It's 7G")
        )
        addPost("conspiracy", secondPosts)
        val response = doSend(body= Search(size = 6), endPoint = "search",
                clazz=PostController.SearchResponse::class.java).body!!
        Assertions.assertEquals(6, response.unseen.size, "Incorrect size")
    }

    @Test
    fun search_loggedIn() {
        var firstPosts = listOf(
                Post(listOf("java","code"),"Java is dead"),
                Post(listOf("kotlin", "code"), "Kotlin is alive"),
                Post(listOf("kotlin", "code"), "Kotlin is alive")
        )
        addPost("random_user", firstPosts)

        val secondPosts = listOf(
                Post(listOf("covid"),"Covid will be back in 2015"),
                Post(listOf("covid"), "covid has nothing to do with 5G"),
                Post(listOf("covid"), "It's 7G")
        )
        addPost("conspiracy", secondPosts)

        doLogin(LoginRequest("random_user", "Password123"), true)
        val response = doSend(body= Search(size = 6) , endPoint = "search",
                clazz=PostController.SearchResponse::class.java).body!!
        Assertions.assertEquals(6, response.unseen.size, "Incorrect size")
    }

    @Test
    fun search_byAccount() {
        var firstPosts = listOf(
                Post(listOf("java","code"),"Java is dead"),
                Post(listOf("kotlin", "code"), "Kotlin is alive")
        )
        firstPosts = addPost("random_user", firstPosts)

        val secondPosts = listOf(
                Post(listOf("covid"),"Covid will be back in 2015"),
                Post(listOf("covid"), "covid has nothing to do with 5G"),
                Post(listOf("covid"), "It's 7G")
        )
        addPost("conspiracy", secondPosts)
        val search = Search(Search.Type.USER, "random_user")
        val response = doSend(body= search, endPoint = "search",
                clazz=PostController.SearchResponse::class.java).body!!
        Assertions.assertEquals(2, response.unseen.size, "Incorrect size")
        val firstCheck = firstPosts[0] == response.unseen[0] || firstPosts[0] == response.unseen[1]
        val secondCheck = firstPosts[1] == response.unseen[0] || firstPosts[1] == response.unseen[1]
        Assertions.assertTrue(firstCheck, "Post not found")
        Assertions.assertTrue(secondCheck, "Post not found")
    }

    @Test
    fun search_byCaption() {
        var firstPosts = listOf(
                Post(listOf("java","code"),"Java is dead"),
                Post(listOf("kotlin", "code"), "Kotlin is alive")
        )
        firstPosts = addPost("random_user", firstPosts)

        val secondPosts = listOf(
                Post(listOf("covid"),"Covid will be back in 2015"),
                Post(listOf("covid"), "covid has nothing to do with 5G"),
                Post(listOf("covid"), "It's 7G")
        )
        addPost("conspiracy", secondPosts)
        val search = Search(Search.Type.CAPTION, "Kotlin")
        val response = doSend(body= search, endPoint = "search",
                clazz=PostController.SearchResponse::class.java).body!!
        Assertions.assertEquals(1, response.unseen.size, "Incorrect size")
        Assertions.assertEquals(firstPosts[1], response.unseen[0], "Wrong post")
    }

    fun addPost(handler: String, posts: List<Post>, create:Boolean = true): MutableList<Post> {
        val response = mutableListOf<Post>()
        if(create) {
            createAccount(handler)
        }
        accountManager.getByHandler(handler).get().let { account->
            posts.forEach {
                it.owner = account
                response.add(postManager.create(it))
            }
        }
        return response
    }

    private fun isPresent(post: Post): Boolean {
        return postManager.get(post.id!!).isPresent
    }

    @AfterEach
    fun afterEach() {
        cookies = mutableListOf()
        postManager.clean()
        super.clean()
    }

}
 **/