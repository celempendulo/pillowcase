package pillowcase

import epsilon.accounts.Account
import epsilon.accounts.AccountManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.*
import pillowcase.api.Urls
import pillowcase.api.controller.AccountController
import pillowcase.api.model.GeneralResponse
import pillowcase.utils.MailManager
import pillowcase.visitor.VisitorManager

abstract class AControllerTest {

    @Autowired lateinit var  restTemplate: TestRestTemplate
    @Autowired lateinit var accountManager: AccountManager
    @Autowired lateinit var visitorManager: VisitorManager
    @Autowired lateinit var  mailManager: MailManager
    private val baseUrl: String = "http://localhost:8080"

    var cookies = mutableListOf<String>()

    private fun updateCookies(newCookies: MutableList<String>) {
        val toDelete = newCookies.filter { it-> it.contains("Max-Age=0") }
        newCookies.addAll(cookies)
        toDelete.forEach { it ->
            val prefix = it.substringBefore("=")
            newCookies.removeIf{ value-> value.startsWith(prefix)}
        }
        cookies = newCookies
    }

    fun doLogin(info: AccountController.LoginRequest, create:Boolean = false, pp:String = "C++"): GeneralResponse {

        if(create) {
            //createAccount(handler = info.handler, password = info.password, pp = pp)
            return doSend(body = AccountController.RegisterRequest(handler = info.handler, pp="PIC",
                    password = info.password), endPoint = Urls.REGISTRATION, clazz = GeneralResponse::class.java).body!!
        }
        return doSend(body = info, endPoint = Urls.LOGIN, clazz = GeneralResponse::class.java).body!!
    }

    fun ensureAccountIsAbsent(handler: String) {
        accountManager.getByHandler(handler).ifPresent {
            accountManager.delete(it.id!!)
        }
    }

    fun createAccount(handler: String, password: String = "Password123", pp: String= "CUP"): Account {
        val account = Account(
                handler = handler,
                password = password,
                pp = pp,
                active = true)
        accountManager.getByHandler(account.handler!!).ifPresent {
            accountManager.delete(it.id!!)
        }
        return accountManager.create(account)
    }

    fun <T> doSend(body:Any="", endPoint: String, method: HttpMethod = HttpMethod.POST,
                   clazz: Class<T>): ResponseEntity<T> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        headers[HttpHeaders.COOKIE] = cookies
        val entity = HttpEntity(body, headers)
        val result = restTemplate.exchange(baseUrl.plus(endPoint), method, entity, clazz)
        println("statues code : "+result.statusCode)

        val newCookies = mutableListOf<String>()
        result.headers[HttpHeaders.SET_COOKIE]?.let { newCookies.addAll(it) }
        updateCookies(newCookies)

        return result
    }

    fun clean() {
        visitorManager.clean()
        mailManager.clean()
        accountManager.clean()
    }
}